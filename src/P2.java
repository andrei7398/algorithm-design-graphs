import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class P2 {
	
	static class Pair{
        int value,visited;
        
        Pair (int value, int visited) {
        	this.value = value;
        	this.visited = visited;
        }
    }
	
	public void floodFillUtil(Pair[][] numbers, int N, int M, int K, int i, int j, int MIN, int MAX, int sign) {
		
	    // Cazuri in care se iese din matrice cu cautarea
	    if (i < 0 || i >= N || j < 0 || j >= M) { 
	        return; 
	    }
	    // Cazuri in care diferenta e prea mare
	    else if (numbers[i][j].value > MIN + K || numbers[i][j].value < MAX - K) {
	        return; 
	    }
	    // Cazul in care am vizitat deja pozitia curenta
	    else if (numbers[i][j].visited == sign) {
	    	return;
	    }
	    // Cazul de baza
	    else {
	    	// Marcam ca vizitat
    	    numbers[i][j].visited = sign;
    	    
    	    // Verificam daca noua valoare nu este un nou MIN/MAX
    	    if (numbers[i][j].value < MIN) {
    	    	MIN = numbers[i][j].value;
    	    }    
    	    else if (numbers[i][j].value > MAX){
    	    	MAX = numbers[i][j].value;
    	    }
    	    
    	    // Recurenta pentru sus,jos,stanga,dreapta 
    	    floodFillUtil(numbers, N, M, K, i+1, j, MIN, MAX, sign); 
    	    floodFillUtil(numbers, N, M, K, i-1, j, MIN, MAX, sign); 
    	    floodFillUtil(numbers, N, M, K, i, j+1, MIN, MAX, sign); 
    	    floodFillUtil(numbers, N, M, K, i, j-1, MIN, MAX, sign); 
	    }
	} 
	
	void floodFill(Pair[][] numbers, int N, int M, int K, int i, int j, int sign) {
		// Initializam MIN/MAX cu pozitia cu care incepem
	    int MIN = numbers[i][j].value;
	    int MAX = numbers[i][j].value;
	    // Apelam functia floodFillUtil
	    floodFillUtil(numbers, N, M, K, i, j, MIN, MAX, sign); 
	} 

    public static void main (String[] args) throws FileNotFoundException {
    	
		Scanner scan = new Scanner(new File("p2.in"));
		
		// Citire numere 
		int N,M,K;
		
		N = scan.nextInt();
		M = scan.nextInt();
		K = scan.nextInt();
		
		Pair[][] numbers = new Pair[N][M];
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < M ; j++) {
				numbers[i][j] = new Pair(scan.nextInt(),0);	
			}
		}
		
		scan.close();
		
		P2 aux = new P2();
		int sign = 1, aux_i = -1, aux_j = -1;

		// Aplicam algoritmul cat timp mai sunt suprafete nevizitate
		do {
			// sign - semnul suprafetei (1-prima suprafata, 2-a doua ...)
			sign += 1;
			
			// Pozitiile de la care incepem prelucrarea suprafetei
			aux_i = -1;
			aux_j = -1;
			for (int i = 0;  i < N; i++) {
				for (int j = 0; j < M; j++) {
					// Am gasit o noua suprafata nevizitata
					if (numbers[i][j].visited == 0) {
						aux_i = i;
						aux_j = j;
						break;
					}
				}
				// Exista suprafata nevizitata
				if (aux_i != -1 && aux_j != -1) {
					break;
				}
			}
			// Exista suprafata nevizitata - aplicam algoritmul pe ea
			if (aux_i != -1 && aux_j != -1) {
				aux.floodFill(numbers, N, M, K, aux_i, aux_j, sign);	
			}
		} while (aux_i != -1 && aux_j != -1);
		
		
		// Vector in care tinem suprafetele (maxim N*M suprafete - cate elemente are matricea)
		int[] surfaces = new int[N*M];
		
		// Calculam suprafetele & gasim suprafata maxima
		int MAX = -1;
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < M; j++) {
				int index = numbers[i][j].visited;
				surfaces[index] += 1;
				if (surfaces[index] > MAX) {
					MAX = surfaces[index];
				}
			}
		}
		
        // Deschid printerul
		PrintWriter writer = new PrintWriter("p2.out");
		
		// Scriu rezultatul
		writer.printf("%d", MAX);
    	
	    // Inchid printerul
	    writer.close();
    }
}
