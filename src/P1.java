import java.util.*;
import java.io.*;

public class P1 {
	
	static class Pair{
        int node, dist;
        
        Pair (int node, int dist) {
        	this.node = node;
        	this.dist = dist;
        }
    }
	
	public static void main(String[] args) throws FileNotFoundException, IOException {
		
		Scanner scan = new Scanner(new File("p1.in"));
		
		int n;
		
		// Tinem distantele intr-o lista de perechi <nod,distanta pana la sursa>
		ArrayList<Pair> nodes = new ArrayList<Pair>();
		
		// Citire numar noduri
		n = scan.nextInt();
		
		// Citire distante intr-o pereche de tip <nod,distanta pana la sursa>
		for (int i = 0; i < n; i++) {
			nodes.add(new Pair(i+1,scan.nextInt()));
		}
		
		scan.close();
		
		// Sortam lista dupa distante, avand lista de perechi, nu pierdem indecsii
		Collections.sort(nodes, new Comparator<Pair>() {

	        @Override
	        public int compare(Pair p1, Pair p2) {
	            return p1.dist - p2.dist;
	        }
	    });
		
		// Lista cu muchiile finale
		ArrayList<Integer> final_nodes = new ArrayList<Integer>();
		
		// Numarul de muchii
		int m = 0;
		
		for (int i = 0; i < n; i++) {
			// Nodul sursa
			if (nodes.get(i).dist == 0) {
				continue;
			}
			// Nodul actual poate fi legat la nodul precedent
			else if (nodes.get(i).dist == nodes.get(i-1).dist + 1) {
				final_nodes.add(nodes.get(i-1).node);
				final_nodes.add(nodes.get(i).node);
				m++;
			}
			// Nodul actual are aceeasi distanta ca cel precedent
			if (nodes.get(i).dist < nodes.get(i-1).dist + 1) {
				// Cautam nodul in vecinii din stanga, care au distante mai mici sau egale
				for (int j = i-1; j >= 0; j--) {
					if (nodes.get(i).dist == nodes.get(j).dist + 1) {
						final_nodes.add(nodes.get(j).node);
						final_nodes.add(nodes.get(i).node);
						// Crestem numarul de muchii gasite
						m++;
						break;
					}
				}
			}
			// Nodul actual nu poate fi legat la nodul precedent, cautam alt nod
			else if (nodes.get(i).dist > nodes.get(i-1).dist + 1) {
				for (int j = i-1; j >= 0; j--) {
					if (nodes.get(i).dist == nodes.get(j).dist + 1) {;
						final_nodes.add(nodes.get(j).node);
						final_nodes.add(nodes.get(i).node);
						// Crestem numarul de muchii gasite
						m++;
						break;
					}
					// m = 0 -> nu exista un astfel de graf
					if (nodes.get(j).dist == 0) {
						m = 0;
						break;
					}
				}
				if (m == 0) {
					break;
				}
			}
		}
		
		PrintWriter writer = new PrintWriter("p1.out", "UTF-8");
		
		// Scriem rezultatul final
		if (m != 0) {
			writer.printf("%d\n", m);
			for (int i = 0; i < final_nodes.size(); i+=2) {
				writer.printf("%d %d\n", final_nodes.get(i), final_nodes.get(i+1));
			}	
		}
		// Nu exista un astfel de graf
		else {
			writer.printf("-1");
		}
		
		writer.close();
		
	}
}
